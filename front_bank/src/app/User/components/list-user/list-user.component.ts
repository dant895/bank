import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';
import { User } from '../../model/User';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  constructor(
    private userService:UserService,
    private router:Router,
    private modal:NgbModal,
    private modalService: ModalService) { }

  ngOnInit(): void {
  }

users?: User[];

  getAllClients(): void {
    this.userService.getUsers().subscribe({
      next: resp => {
        if(resp.success === 0){
          this.users=resp.data;
        }
        else{
          this.modalService.ResultOperation = String(resp.msgSpanish);
          this.modal.open(ModalComponent,{animation:true, centered:true});
        }
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

}
