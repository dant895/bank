import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';
import { User } from '../../model/User';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
})
export class EditUserComponent implements OnInit {
  user: User = {
    userName: '',
    password: '',
    firstName: '',
    lastName: '',
    jwt: '',
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private modal: NgbModal,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {}

  updateUser(): void {
    this.route.paramMap.subscribe((params) => {
      if (params.has('userName')) {
        this.userService
          .updateUser(params.get('userName'), this.user)
          .subscribe({
            next: (resp) => {
              this.modalService.ResultOperation = String(resp.msgSpanish);
              this.modal.open(ModalComponent, {
                animation: true,
                centered: true,
              });
              this.router.navigate(['init']);
            },
            error: (e) => console.error(e),
          });
      }
    });
  }
}
