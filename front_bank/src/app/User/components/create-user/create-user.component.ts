import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';
import { User } from '../../model/User';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  user: User = {
    userName: '',
    password: '',
    firstName: '',
    lastName: '',
    jwt: '',

  };

  constructor(
    private router: Router,
    private userService: UserService,
    private modal: NgbModal,
    private modalService: ModalService) { }

  ngOnInit(): void {
  }

  signUp(){
    const data = {
      userName: this.user.userName,
      password: this.user.password,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      jwt: '',

    };

    this.userService.createUser(data).subscribe({
      next: (resp) => {
        this.modalService.ResultOperation = String(resp.msgSpanish);
        this.modal.open(ModalComponent,{animation:true, centered:true});
        if (resp.success === 0) {
          this.router.navigate(['']);
        }
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

}
