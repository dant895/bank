import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralResponse } from '../../shared/models/general-response';
import { User } from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  UrlUsers='http://localhost:8080/users/';

  constructor(private http: HttpClient) { }

  //List all the users of the DB
  getUsers(): Observable<GeneralResponse<User[]>>{
    return this.http.get<GeneralResponse<User[]>>(this.UrlUsers);
  }

  //Create a user
  createUser(users: User): Observable<GeneralResponse<User>> {
    return this.http.post<GeneralResponse<User>>(this.UrlUsers, users);
  }

  //Update user info
  updateUser(userName: string | null, user: User): Observable<any> {
    const url = `${this.UrlUsers}${userName}`
    return this.http.put<GeneralResponse<User>>(url, user);
  }

  //Delete user
  deleteUser(userName: string | null): Observable<GeneralResponse<string>> {
    const url = `${this.UrlUsers}${userName}`
    return this.http.delete<GeneralResponse<string>>(url);
  }

  //SigIn in the application with username and password
  login(user: User): Observable<GeneralResponse<User>> {
    const url = `${this.UrlUsers}auth`
    return this.http.post<GeneralResponse<User>>(url, user);
  }


}
