import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GeneralResponse } from 'src/app/shared/models/general-response';
import { ModalService } from './service/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {


  constructor(public activeModal: NgbModal,
              public modalService: ModalService) { }

  ngOnInit(): void {
  }

}
