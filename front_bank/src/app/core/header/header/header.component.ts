import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/service/global.service';
import { User } from 'src/app/User/model/User';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLogged = false;
  nameUser: string = '';
  lastnameUser: string = '';

  constructor(
    private router:Router,
    private globalService: GlobalService) { }




    ngOnInit(): void {
  }

  isAuthenticated (user: User){
    this.globalService.user = user;
    this.isLogged = true;
    this.nameUser = user.firstName;
    this.lastnameUser = user.lastName;
  }

  signOut (){
    // this.globalService.user = null;
    this.router.navigate(['']);
    this.isLogged = false;

  }


  goToChangePassword() : void{
    const userName = this.globalService.user.userName;
    this.router.navigate(["users/",userName]);
  }

  goToListCustomers(){
    this.router.navigate(["customers"]);
  }

  goToCreateCustomer(){
    this.router.navigate(["addCustomer"]);
  }

}
