import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/User/model/User';
import { UserService } from 'src/app/User/service/user.service';
import { ModalComponent } from '../modal/modal.component';
import { ModalService } from '../modal/service/modal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() login: EventEmitter<User> = new EventEmitter();

  user: User = {
    userName: '',
    password: '',
    firstName: '',
    lastName: '',
    jwt: '',

  };

  newUser: User = {
    userName: '',
    password: '',
    firstName: '',
    lastName: '',
    jwt: '',

  };

  validateLogin:boolean = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private modal:NgbModal,
    private modalService: ModalService
  ) { }

  ngOnInit(): void {
  }

  signIn(){
    this.validateLogin = true;
    this.userService.login(this.user).subscribe({
      next: resp => {
        console.log(resp);

        if (resp.success === 0) {
          this.validateLogin = false;
          this.login.emit(resp.data);
          this.router.navigate(['init']);
        }
        else if(resp.success === 1) {
          this.validateLogin = false;
        } else {
          this.validateLogin = false;
        }
      }, error: (error) => {
        console.error(error);
      },
    });

  }


  goToSignUp(): void{
    this.validateLogin = true;
    this.newUser.userName = '';
    this.newUser.password = '';
    this.newUser.firstName = '';
    this.newUser.lastName = '';
  }

  signUp(){
    const data = {
      userName: this.newUser.userName,
      password: this.newUser.password,
      firstName: this.newUser.firstName,
      lastName: this.newUser.lastName,
      jwt: '',

    };

    this.userService.createUser(data).subscribe({
      next: (resp) => {
        this.modalService.ResultOperation = String(resp.msgSpanish);
        this.modal.open(ModalComponent,{animation:true, centered:true});
        if (resp.success === 0) {
          this.router.navigate(['']);
          this.validateLogin = false;
        }
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  cancel(){
    this.validateLogin = false;
    this.user.userName = '';
    this.user.password = '';
  }

}
