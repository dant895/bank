import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Client } from 'src/app/Client/model/Client';
import { ClientService } from 'src/app/Client/service/client.service';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.css']
})
export class ListClientComponent implements OnInit {

  clients?:Client[];
  currentClient: Client ={};
  currentIndex = -1;


  constructor(
    private clientService:ClientService,
    private router:Router,
    private modal:NgbModal,
    private modalService: ModalService) { }

  ngOnInit(): void {
    this.getAllClients();
  }

  getAllClients(): void {
    this.clientService.getClient().subscribe({
      next: resp => {
        if(resp.success === 0){
          this.clients=resp.data;
        }
        else{
          this.modalService.ResultOperation = String(resp.msgSpanish);
          this.modal.open(ModalComponent,{animation:true, centered:true});
        }
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  setActiveClient(client: Client): void{
    this.currentClient = client;
  }

  goToEditClient(idClient: number | undefined): void{
    this.router.navigate(["customers/",idClient,"accounts"]);
  }

}
