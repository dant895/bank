import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Client } from 'src/app/Client/model/Client';
import { ClientService } from 'src/app/Client/service/client.service';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {

  // currentClient: Client;

    currentClient: Client = {
    typeId: '',
    numberId: '',
    lastname: '',
    name: '',
    email: '',
    dateBirth: '',
    telephone: '',
    dateCreation: '',
  };

  constructor(
    private clientService: ClientService,
    private route: ActivatedRoute,
    private router: Router,
    private modal: NgbModal,
    private modalService: ModalService,
  ) { }

  ngOnInit(): void {
    this.getClientbyId();
  }

  getClientbyId(): void {
    this.route.paramMap.subscribe(params=> {
      if (params.has("id")){
        this.clientService.getClientId(params.get("id")).subscribe({
          next: (resp) => {
            if(resp.success === 0){
              this.currentClient = resp.data;
            }
            else{
              this.modalService.ResultOperation = String(resp.msgSpanish);
              this.modal.open(ModalComponent,{animation:true, centered:true});
            }
          },
          error: (error) => {
            console.error(error);
          },
        });
      }
    });
  }

  updateClient(): void{
    this.clientService.updateClient(this.currentClient.id, this.currentClient)
    .subscribe({
      next: (resp) => {
        this.modalService.ResultOperation = String(resp.msgSpanish);
        this.modal.open(ModalComponent,{animation:true, centered:true});
      },
      error: (e) => console.error(e)
    });
  }

  deleteClient(): void{
    this.clientService.deleteClient(this.currentClient.id)
    .subscribe({
      next: (resp) => {
        this.modalService.ResultOperation = String(resp.msgSpanish);
        this.modal.open(ModalComponent,{animation:true, centered:true});
        if (resp.success === 0){
          this.router.navigate(['/customers']);
        }

      },
      error: (e) => console.error(e)
    });
  }

  backClient(): void{
    this.router.navigate(['/customers']);
  }

}
