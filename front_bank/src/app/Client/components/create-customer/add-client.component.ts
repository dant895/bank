import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Client } from 'src/app/Client/model/Client';
import { ClientService } from 'src/app/Client/service/client.service';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css'],
})
export class AddClientComponent implements OnInit {
  dateNow = new Date();

  client: Client = {
    typeId: '',
    numberId: '',
    lastname: '',
    name: '',
    email: '',
    dateBirth: '',
    telephone: '',
    dateCreation: '',
  };

  constructor(
    private router: Router,
    private clientService: ClientService,
    private modal:NgbModal,
    private modalService: ModalService,) {}

  ngOnInit(): void {}

  saveClient(): void {
    const data = {
      typeId: this.client.typeId,
      numberId: this.client.numberId,
      lastname: this.client.lastname,
      name: this.client.name,
      email: this.client.email,
      dateBirth: this.client.dateBirth,
      telephone: this.client.telephone,
      dateCreation: formatDate(this.dateNow, 'YYYY-MM-dd', 'en-US'),
    };

    this.clientService.createClient(data).subscribe({
      next: (resp) => {
        this.modalService.ResultOperation = String(resp.msgSpanish);
        this.modal.open(ModalComponent,{animation:true, centered:true});

        if (resp.success === 0) {
          this.router.navigate(['customers']);
        }
      },
      error: (error) => {
        console.error(error);
      },
    });
  }
}
