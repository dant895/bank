import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Client } from 'src/app/Client/model/Client';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';
import { Product } from 'src/app/Product/model/Product';
import { ProductService } from 'src/app/Product/product-service/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
})
export class AddProductComponent implements OnInit {
  dateNow = new Date();

  products?: Product[];
  clients?: Client[];

  product: Product = {
    typeAccount: '',
    numberAccount: '',
    creationDate: '',
    state: '',
    balance: 0,
  };
  save = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    private modal: NgbModal,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {}

  saveProduct(): void {
    const data = {
      typeAccount: this.product.typeAccount,
      numberAccount: this.product.numberAccount,
      state: 'activa',
      creationDate: formatDate(this.dateNow, 'YYYY-MM-dd', 'en-US'),
    };

    this.route.paramMap.subscribe((params) => {
      if (params.has('id')) {
        this.productService
          .getProduct(params.get('id'))
          .subscribe((resp) => (this.products = resp.data));
        this.productService.createProduct(data, params.get('id')).subscribe({
          next: (resp) => {
            this.modalService.ResultOperation = String(resp.msgSpanish);
            this.modal.open(ModalComponent,{animation:true, centered:true});
            if (resp.success === 0){
              this.router.navigate(['customers', params.get('id'), 'accounts']);
            }
          },
          error: (error) =>{
            console.error(error);
          },
        });
      }
    });
  }

  backProduct(): void {
    this.route.paramMap.subscribe((params) => {
      this.router.navigate(['customers', params.get('id'), 'accounts']);
    });

  }
}
