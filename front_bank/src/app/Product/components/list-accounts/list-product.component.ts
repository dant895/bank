import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/Product/model/Product';
import { ClientService } from 'src/app/Client/service/client.service';
import { ProductService } from 'src/app/Product/product-service/product.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';

@Component({
  selector: 'app-list-product',
  template: `
    <app-modal [modalMessage]="msgSpanish"></app-modal>
  `,
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {

  products?:Product[];
  currentProduct: Product = {};
  currentIndex = -1;

  constructor(private productService:ProductService,
    private clientService: ClientService,
    private route: ActivatedRoute,
    private router:Router,
    private modal: NgbModal,
    private modalService: ModalService,
    ) { }

  ngOnInit(): void {
    this.listAllProducts();
  }

  listAllProducts():void {
    this.route.paramMap.subscribe(params=> {
      if (params.has("id")){
        this.productService.getProduct(params.get("id")).subscribe({
          next: (resp) => {
            if(resp.success === 0){
              this.products = resp.data;
            }
            else{
              this.modalService.ResultOperation = String(resp.msgSpanish);
              this.modal.open(ModalComponent,{animation:true, centered:true});

            }
          },
          error: (error) => {
            console.error(error);
          },
        });
      }
    })
  }

  goToCreateAccount(): void{
    this.route.paramMap.subscribe(params=> {
      if (params.has("id")){
        this.router.navigate(["customers/",params.get("id"),"accounts","add"]);
      }
    })

  }


  goToCreateTransaction(idClient:any, idProduct:any): void{
    this.router.navigate(["customers/",idClient,"accounts",idProduct,"transaction"])
  }

  goToAccountStatus(idClient:any, idProduct:any): void{
    this.router.navigate(["customers/",idClient,"accounts",idProduct])

  }

  changeStatus(idClient:any, idProduct:any): void{
    this.route.paramMap.subscribe(params=> {
      if (params.has("id")){
        this.productService.updateStatusProduct(idClient, idProduct, this.currentProduct).subscribe({
        next: (resp) => {
          this.modalService.ResultOperation = String(resp.msgSpanish);
          this.modal.open(ModalComponent,{animation:true, centered:true});
          this.productService.getProduct(params.get("id")).subscribe(respu =>this.products = respu.data);
          this.router.navigate(["customers/",params.get("id"),"accounts"]);
        },
        error: (error) => {
          console.error(error);
        },
        });
      }}
    );

  }

  cancelProduct(idClient:any, idProduct:any): void{
    this.route.paramMap.subscribe(params=> {
      if (params.has("id")){
        this.productService.cancelStatusProduct(params.get("id"), idProduct, this.currentProduct).subscribe({
        next: (resp) => {
          this.modalService.ResultOperation = String(resp.msgSpanish);
          this.modal.open(ModalComponent,{animation:true, centered:true});
          this.productService.getProduct(params.get("id")).subscribe(respu =>this.products = respu.data);
          this.router.navigate(["customers/",params.get("id"),"accounts"]);
        },
        error: (error) => {
          console.error(error);
        },
        });
      }}
    );

  }

}
