export class GeneralResponse<T> {
  data?: T;
  success?: number;
  message?: string;
  msgSpanish?: String;
}
