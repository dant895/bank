import { Injectable } from "@angular/core";
import { User } from "src/app/User/model/User";


@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    public user: User = new User();

    constructor() {

    }

}
