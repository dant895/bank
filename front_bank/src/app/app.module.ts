import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ClientService} from './Client/service/client.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LandingComponent } from './core/landing/landing.component';
import { AddClientComponent } from './Client/components/create-customer/add-client.component';
import { EditClientComponent } from './Client/components/edit-customer/edit-client.component';
import { ListClientComponent } from './Client/components/list-customers/list-client.component';
import { ListProductComponent } from './Product/components/list-accounts/list-product.component';
import { AddProductComponent } from './Product/components/create-account/add-product.component';
import { ListTransactionComponent } from './Transaction/components/list-transaction/list-transaction.component';
import { CreateTransactionComponent } from './Transaction/components/create-transaction/create-transaction.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './core/header/header/header.component';
import { ModalComponent } from './core/modal/modal.component';
import { LoginComponent } from './core/login/login.component';
import { CreateUserComponent } from './User/components/create-user/create-user.component';
import { ListUserComponent } from './User/components/list-user/list-user.component';
import { EditUserComponent } from './User/components/edit-user/edit-user.component';
import { HttpConfigInterceptor } from './shared/http-config-interceptor';
import { UserService } from './User/service/user.service';
import { ProductService } from './Product/product-service/product.service';
import { TransactionService } from './Transaction/service/transaction.service';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    AddClientComponent,
    EditClientComponent,
    ListClientComponent,
    ListProductComponent,
    AddProductComponent,
    ListTransactionComponent,
    CreateTransactionComponent,
    HeaderComponent,
    ModalComponent,
    LoginComponent,
    CreateUserComponent,
    ListUserComponent,
    EditUserComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [
    ClientService,
    UserService,
    ProductService,
    TransactionService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpConfigInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent],
  exports: [
    LoginComponent
  ]
})
export class AppModule { }
function routes(routes: any, arg1: { onSameUrlNavigation: "reload"; }): any[] | import("@angular/core").Type<any> | import("@angular/core").ModuleWithProviders<{}> {
  throw new Error('Function not implemented.');
}

