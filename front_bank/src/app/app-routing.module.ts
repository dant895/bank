import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddClientComponent } from './Client/components/create-customer/add-client.component';
import { EditClientComponent } from './Client/components/edit-customer/edit-client.component';
import { ListClientComponent } from './Client/components/list-customers/list-client.component';
import { LandingComponent } from './core/landing/landing.component';
import { AddProductComponent } from './Product/components/create-account/add-product.component';
import { ListTransactionComponent } from './Transaction/components/list-transaction/list-transaction.component';
import { CreateTransactionComponent } from './Transaction/components/create-transaction/create-transaction.component';
import { LoginComponent } from './core/login/login.component';
import { CreateUserComponent } from './User/components/create-user/create-user.component';
import { EditUserComponent } from './User/components/edit-user/edit-user.component';


const routes: Routes = [
  // //Login
  // {path:'', component: LoginComponent},
  //Create a user - signUp
  {path:'users/:userName', component: EditUserComponent},
  //Create a user - signUp
  {path:'signup', component: CreateUserComponent},
  //Landing page
  {path:'init', component:LandingComponent},
  // To list all the clients
  {path:'customers', component:ListClientComponent},
  // To create a client
  {path:'addCustomer', component:AddClientComponent},
  // To edit a client info
  {path:'customers/:id/accounts', component:EditClientComponent},
  //To create a product
  {path:'customers/:id/accounts/add', component:AddProductComponent},
  //To list all the transaction of a product
  {path:'customers/:id/accounts/:idProduct', component:ListTransactionComponent},
  //To create a transaction
  {path:'customers/:id/accounts/:idProduct/transaction', component:CreateTransactionComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
