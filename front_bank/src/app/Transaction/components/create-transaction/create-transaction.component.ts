import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';
import { Product } from 'src/app/Product/model/Product';
import { Transaction } from 'src/app/Transaction/model/Transaction';
import { TransactionService } from 'src/app/Transaction/service/transaction.service';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.css']
})
export class CreateTransactionComponent implements OnInit {


  dateNow = new Date();

  transaction: Transaction={
    idSecondaryProduct:0,
    typeOperation: '' ,
    valueOperation:0 ,
    dateOperation: '',
    description:'',
    resultOperation:'',
    finalBalance:0 ,
    GMF:0 ,
    financeMovement:'',
  };

  isCollapsed(): boolean {
    if (this.transaction.typeOperation != 'Transferencia' )
      return true;
    else
      return false;
  }

  products?: Product[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private transactionService: TransactionService,
    private modal: NgbModal,
    private modalService: ModalService,
  ) { }

  ngOnInit(): void {
    this.listProductsToTransfer();

  }


  listProductsToTransfer(): void {
    this.route.paramMap.subscribe(params=> {
      if (params.has("id")){
        this.transactionService.getProductsToTransfer(params.get("id"),params.get("idProduct")).subscribe(resp =>this.products = resp.data);
      }
    })
  }

  data = {
    idSecondaryProduct:this.transaction.idSecondaryProduct,
    typeOperation: this.transaction.typeOperation ,
    valueOperation:this.transaction.valueOperation ,
    dateOperation: formatDate(this.dateNow, 'YYYY-MM-dd', 'en-US'),
    description:this.transaction.description,
  };


  makeTransaction(): void {

    const data = {
      idSecondaryProduct:this.transaction.idSecondaryProduct,
      typeOperation: this.transaction.typeOperation ,
      valueOperation:this.transaction.valueOperation ,
      dateOperation: formatDate(this.dateNow, 'YYYY-MM-dd', 'en-US'),
      description:this.transaction.description,
    };

    if (this.transaction.typeOperation === "Consignación"){
      this.addMoney(data);
    }
    else if (this.transaction.typeOperation === 'Retiro'){
      this.withdrawMoney(data);
    }
    else if (this.transaction.typeOperation === 'Transferencia'){
      this.transferMoney(data);
    }
  }

  addMoney(data: Transaction): void {
    this.route.paramMap.subscribe((params) => {
      this.transactionService.addMoney(data, params.get('id'), params.get('idProduct')).subscribe(
        {
          next: resp => {

            this.modalService.ResultOperation = String(resp.msgSpanish);
            this.modal.open(ModalComponent,{animation:true, centered:true});
            if(resp.success === 0){
              this.router.navigate(['customers', params.get('id'), 'accounts']);
            }
          },
          error: (error) => {
            console.error(error);
          },
        }
      )
      })
  }


  withdrawMoney(data: Transaction): void {
    this.route.paramMap.subscribe((params) => {
      this.transactionService.withdrawMoney(data, params.get('id'), params.get('idProduct')).subscribe(
        {
          next: resp => {
            this.modalService.ResultOperation = String(resp.msgSpanish);
            this.modal.open(ModalComponent,{animation:true, centered:true});
            if(resp.success === 0){
              this.router.navigate(['customers', params.get('id'), 'accounts']);
            }
          },
          error: (error) => {
            console.error(error);
          },
        }
      )
      })
  }


  transferMoney(data: Transaction): void {
    this.route.paramMap.subscribe((params) => {
      this.transactionService.transferMoney(data, params.get('id'), params.get('idProduct')).subscribe(
        {
          next: resp => {
            this.modalService.ResultOperation = String(resp.msgSpanish);
            this.modal.open(ModalComponent,{animation:true, centered:true});
            if(resp.success === 0){
              this.router.navigate(['customers', params.get('id'), 'accounts']);
            }
          },
          error: (error) => {
            console.error(error);
          },
        }
      )
      })
  }


  backToProduct(): void {
    this.route.paramMap.subscribe((params) => {
      this.router.navigate(['customers', params.get('id'), 'accounts']);
    });

  }

}


