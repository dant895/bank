import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Transaction } from 'src/app/Transaction/model/Transaction';
import { ClientService } from 'src/app/Client/service/client.service';
import { ProductService } from 'src/app/Product/product-service/product.service';
import { TransactionService } from 'src/app/Transaction/service/transaction.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { ModalService } from 'src/app/core/modal/service/modal.service';

@Component({
  selector: 'app-list-transaction',
  templateUrl: './list-transaction.component.html',
  styleUrls: ['./list-transaction.component.css']
})
export class ListTransactionComponent implements OnInit {

  transactions?: Transaction[];

  constructor(private productService:ProductService,
    private clientService: ClientService,
    private transactionService: TransactionService,
    private route: ActivatedRoute,
    private router:Router,
    private modal:NgbModal,
    private modalService: ModalService) { }

  ngOnInit(): void {
    this.listTransactions();
  }

  listTransactions(): void {
    this.route.paramMap.subscribe(params=> {
      if (params.has("id")){
        this.transactionService.getTransaction(params.get("id") ,params.get("idProduct")).subscribe({
          next: resp => {
            if (resp.success === 0){
              this.transactions = resp.data;
            }
            else{
              this.modalService.ResultOperation = String(resp.msgSpanish);
              this.modal.open(ModalComponent,{animation:true, centered:true, size:'lg'});
              this.router.navigate(['customers', params.get('id'), 'accounts']);
            }
          },
          error: (error) => {
            console.error(error);
          },
        });
      }
    })
  }

  backProduct(): void {
    this.route.paramMap.subscribe((params) => {
      this.router.navigate(['customers', params.get('id'), 'accounts']);
    });

  }
}
