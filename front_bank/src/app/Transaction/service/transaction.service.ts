import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../../Product/model/Product';
import { Transaction } from '../model/Transaction';
import { GeneralResponse } from '../../shared/models/general-response';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  UrlTransaction = 'http://localhost:8080/customers/';

  constructor(private http: HttpClient) {}

  //List the transaction for the product
  getTransaction(
    id: string | null,
    idProduct: string | null
  ): Observable<GeneralResponse<Transaction[]>> {
    const url = `${this.UrlTransaction}${id}/accounts/${idProduct}`;
    return this.http.get<GeneralResponse<Transaction[]>>(url);
  }

  //Create a transaction for a product
  createTransaction(
    data: Transaction,
    id: string | null,
    idProduct: string | null
  ): Observable<GeneralResponse<Transaction>> {
    const url = `${this.UrlTransaction}${id}/accounts/${idProduct}`;
    return this.http.post(url, data);
  }

  // Add money
  addMoney(
    data: Transaction,
    id: string | null,
    idProduct: string | null
  ): Observable<GeneralResponse<Transaction>> {
    const url = `${this.UrlTransaction}${id}/accounts/${idProduct}/add`;
    return this.http.post(url, data);
  }

  // Withdraw money
  withdrawMoney(
    data: Transaction,
    id: string | null,
    idProduct: string | null
  ): Observable<GeneralResponse<Transaction>> {
    const url = `${this.UrlTransaction}${id}/accounts/${idProduct}/withdrawal`;
    return this.http.post(url, data);
  }

  // Transfer money
  transferMoney(
    data: Transaction,
    id: string | null,
    idProduct: string | null
  ): Observable<GeneralResponse<Transaction>> {
    const url = `${this.UrlTransaction}${id}/accounts/${idProduct}/transfer`;
    return this.http.post(url, data);
  }

  //List all the products that are able to transfer money
  getProductsToTransfer(
    id: string | null,
    idProduct: string | null
  ): Observable<GeneralResponse<Product[]>> {
    const url = `${this.UrlTransaction}${id}/accounts/${idProduct}/different`;
    return this.http.get<GeneralResponse<Product[]>>(url);
  }
}
