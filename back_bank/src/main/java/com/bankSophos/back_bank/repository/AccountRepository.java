package com.bankSophos.back_bank.repository;

import com.bankSophos.back_bank.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {

    List<AccountEntity> findByIdClient(int idClient);

    AccountEntity findByIdProduct(int idProduct);

    @Query(value = "select * from products where (state!=?1) AND NOT (id_client=?2 and id_product=?3)", nativeQuery = true)
    List<AccountEntity> findByStateNotAndIdClientNotAndIdProductNot(String state, int idClient, int idProduct);
}