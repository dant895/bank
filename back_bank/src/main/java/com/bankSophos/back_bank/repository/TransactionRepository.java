package com.bankSophos.back_bank.repository;

import com.bankSophos.back_bank.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {

    List<TransactionEntity> findByIdPrincipalProductAndResultOperation(int idPrincipalProduct, String resultOperation);

}
