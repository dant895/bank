package com.bankSophos.back_bank.entity;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class UserEntity {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int idUser;
    @Id
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String jwt;

    public UserEntity() {
    }

    public UserEntity(String userName, String jwt) {
        this.userName = userName;
        this.jwt = jwt;
    }

    public UserEntity( String username, String password, String firstName, String lastName, String jwt) {
//        this.idUser = idUser;
        this.userName = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.jwt = jwt;
    }

//    public int getIdUser() {
//        return idUser;
//    }
//
//    public void setIdUser(int idUser) {
//        this.idUser = idUser;
//    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
