package com.bankSophos.back_bank.controller;

import com.bankSophos.back_bank.entity.UserEntity;
import com.bankSophos.back_bank.model.GeneralResponse;
import com.bankSophos.back_bank.service.UserService;
import com.bankSophos.back_bank.util.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/users")
public class UserController {

    public static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtutils;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    //Login with userName and password
    @PostMapping("/auth")
    public ResponseEntity<GeneralResponse<UserEntity>> login(@RequestBody UserEntity user){
        GeneralResponse<UserEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        UserEntity userLogin = null;

        try {
            userLogin = getUsers(user).getBody().getData();
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword())
            );
            user.setJwt(jwtutils.generateToken(user.getUserName()));
            user.setPassword(null);
            user.setFirstName(userLogin.getFirstName());
            user.setLastName(userLogin.getLastName());

            String msg = "Login success";
            String msgSpanish = "Acceso autorizado";

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setSuccess(0);
            response.setData(user);
            status = HttpStatus.OK;

        }
        catch (AuthenticationException e){
            String msg = "User or password incorrect";
            String msgSpanish = "Usuario o clave incorrectas";

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setSuccess(1);

            status = HttpStatus.FORBIDDEN;
        }
        catch (Exception e){
            String msg = "Something has failed. Please contact support.";
            String msgSpanish = "Error del sistema. Por favor contacte a Soporte" + e.getMessage() + " " + e.getLocalizedMessage();

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setSuccess(4);

            status = HttpStatus.INTERNAL_SERVER_ERROR;

        }
        return new ResponseEntity<>(response, status);
    }

    //Listing users
    @GetMapping("")
    public ResponseEntity<GeneralResponse<UserEntity>> getUsers(UserEntity user) {

        GeneralResponse<UserEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        UserEntity data = null;

        try {

            data = userService.getUser(user.getUserName());
            String msg;
            String msgSpanish;
            if (data != null){
                msg = "User found";
                msgSpanish = "Usuarios encontrados";
                response.setSuccess(0);
            } else {
                msg = "There was not found user.";
                msgSpanish = "No se encontraron usuarios";
                response.setSuccess(1);
            }

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);

            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact suuport.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);
    }

    //Create users
    @PostMapping
    public ResponseEntity<GeneralResponse<UserEntity>> saveUser(@RequestBody UserEntity user) {

        GeneralResponse<UserEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        UserEntity data = null;

        try {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            data = userService.save(user);
            String msg;
            String msgSpanish;
            if (data != null){
                msg = "User created";
                msgSpanish = "Usuario creado";
                response.setSuccess(0);
            } else {
                msg = "User not created";
                msgSpanish = "Error al crear usuario. Usuario no creado";
                response.setSuccess(1);
            }

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support." + e.getLocalizedMessage() + e.getMessage();
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);
        }

        return new ResponseEntity<>(response, status);
    }

    //Update user
    @PutMapping("/{userName}")
    public ResponseEntity<GeneralResponse<UserEntity>> updateUser(@RequestBody UserEntity user, @PathVariable("userName") String username) {

        GeneralResponse<UserEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        UserEntity data = null;

        try {
            user.setUserName(username);
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            data = userService.save(user);
            String msg;
            String msgSpanish;
            if (data != null){
                msg = "User successfully updated";
                msgSpanish = "Información del usuario actualizada";
                response.setSuccess(0);
            } else {
                msg = "Something went wrong updating the user";
                msgSpanish = "Error al actualizar el usuario";
                response.setSuccess(1);
            }

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }

        return new ResponseEntity<>(response, status);
    }

    //Delete user
    @DeleteMapping("/{userName}")
    public ResponseEntity<GeneralResponse<String>> delete(@PathVariable String userName) {

        GeneralResponse<String> response = new GeneralResponse<>();
        HttpStatus status = null;

        try {

            userService.delete(userName);
            String msg = "User " + userName + " successfully deleted.";
            String msgSpanish = "Usuario eliminado exitosamente";

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setSuccess(0);
            response.setData(userName);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }

        return new ResponseEntity<>(response, status);
    }





}
