package com.bankSophos.back_bank.controller;

import com.bankSophos.back_bank.model.GeneralResponse;
import com.bankSophos.back_bank.service.CustomerService;
import com.bankSophos.back_bank.service.AccountService;
import com.bankSophos.back_bank.entity.CustomerEntity;
import com.bankSophos.back_bank.entity.AccountEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/customers")
public class CustomerController {

    public static final Logger logger = LoggerFactory.getLogger(CustomerController.class);


    @Autowired
    CustomerService customerService;

    @Autowired
    AccountService accountService;

    //Listing all the clients
    @GetMapping("")
    public ResponseEntity<GeneralResponse<List<CustomerEntity>>> list()
    {
        GeneralResponse<List<CustomerEntity>> response = new GeneralResponse<>();
        HttpStatus status = null;
        List<CustomerEntity> data = null;

        try {
            data = customerService.list();
            String msg;
            String msgSpanish;
            if (data.size() > 0){
                msg = "There was found " + data.size() + " customers";
                msgSpanish = "Clientes listados existosamente";
                response.setSuccess(0);
            } else {
                msg = "There was not found customers";
                msgSpanish = "No hay clientes listados en la base de datos";
                response.setSuccess(1);
            }
            response.setMsgSpanish(msgSpanish);
            response.setMessage(msg);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);
        }
        return new ResponseEntity<>(response, status);
    }

    //Create a new client
    @PostMapping("")
    @ResponseBody
    public ResponseEntity<GeneralResponse<CustomerEntity>> save(@RequestBody CustomerEntity customer){

        GeneralResponse<CustomerEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        CustomerEntity data = null;

        try {
            data = customerService.add(customer);
            String msg;
            String msgSpanish;
            if (data != null){
                msg = "The customer was created with the id " + data.getId() + ".";
                msgSpanish = "Cliente creado existosamente";
                response.setSuccess(0);
            } else {
                msg = "The customer was not created. Please review data";
                msgSpanish = "Error al crear el cliente. por favor revise la información suministrada";
                response.setSuccess(1);
            }
            response.setMsgSpanish(msgSpanish);
            response.setMessage(msg);
            response.setData(data);
            status = HttpStatus.CREATED;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }

        return new ResponseEntity<>(response, status);

    }

    //List just one client by its Id
    @GetMapping("/{id}")
    public ResponseEntity<GeneralResponse<CustomerEntity>> listIdOneClient(@PathVariable("id") int id){
        GeneralResponse<CustomerEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        CustomerEntity data = null;

        try {
            data = customerService.listIdOneClient(id);
            String msg;
            String msgSpanish;
            if (data != null){
                msg = "The customer with the id " + data.getId() + " was found.";
                msgSpanish = "Cliente encontrado";
                response.setSuccess(0);
            } else {
                msg = "The customer with the id " + data.getId() + " was not found.";
                msgSpanish = "Cliente no encontrado";
                response.setSuccess(1);
            }
            response.setMsgSpanish(msgSpanish);
            response.setMessage(msg);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }

        return new ResponseEntity<>(response, status);

    }

    //Edit client info of the id selected
    @PutMapping("/{id}")
    public ResponseEntity<GeneralResponse<CustomerEntity>> edit(@RequestBody CustomerEntity customer, @PathVariable("id") int id){

        GeneralResponse<CustomerEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        CustomerEntity data = null;

        try {
            customer.setId(id);
            data = customerService.edit(customer);
            String msg;
            String msgSpanish;
            if (data != null){
                msg = "Customer " + data.getId() + " successfully updated";
                msgSpanish = "Cliente actualizado exitosamente";
                response.setSuccess(0);
            } else {
                msg = "The customer with the id " + data.getId() + " was not updated";
                msgSpanish = "Cliente no actualizado";
                response.setSuccess(1);
            }
            response.setMsgSpanish(msgSpanish);
            response.setMessage(msg);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }

    //Delete a client info
    @DeleteMapping("/{id}")
    public ResponseEntity<GeneralResponse<Integer>>  delete(@PathVariable("id") int id){

        GeneralResponse<Integer> response = new GeneralResponse<>();
        HttpStatus status = null;

        try {

            List<AccountEntity> product = accountService.listIdProduct(id);
            int count_state = 0;
            for (AccountEntity model: product){
                if (model.getState().equals("activa") || model.getState().equals("inactiva")){
                    count_state ++;
                }
            }
            String msg;
            String msgSpanish;
            if (count_state > 0){
                msg = "The customer cannot be deleted because there are " + count_state + " valid accounts.";
                msgSpanish = "Cliente no eliminado. El cliente aún posee cuentas vigentes";
                response.setSuccess(1);
            }
            else {
                msg = "Customer id " + id + "deleted";
                msgSpanish = "Cliente eliminado exitosamente";
                response.setSuccess(0);
                customerService.delete(id);
            }
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(id);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);
        }
        return new ResponseEntity<>(response, status);
    }





}
