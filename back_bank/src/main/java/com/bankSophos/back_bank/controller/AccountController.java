package com.bankSophos.back_bank.controller;

import com.bankSophos.back_bank.entity.CustomerEntity;
import com.bankSophos.back_bank.model.GeneralResponse;
import com.bankSophos.back_bank.service.AccountService;
import com.bankSophos.back_bank.service.TransactionService;
import com.bankSophos.back_bank.entity.AccountEntity;
import com.bankSophos.back_bank.entity.TransactionEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping
public class AccountController {

    public static final Logger logger = LoggerFactory.getLogger(CustomerController.class);


    @Autowired
    AccountService accountService;

    @Autowired
    TransactionService transactionService;

    //List products owned by the client
    @GetMapping("/customers/{idClient}/accounts")
    public ResponseEntity<GeneralResponse<List<AccountEntity>>> listIdProduct(@PathVariable("idClient") int idClient){
        GeneralResponse<List<AccountEntity>> response = new GeneralResponse<>();
        HttpStatus status = null;
        List<AccountEntity> data = null;

        try {
            data = accountService.listIdProduct(idClient);
            String msg;
            String msgSpanish;
            if (data.size()>0) {
                msg = "There was found " + data.size() + " accounts for the customer " + idClient;
                msgSpanish = "Lista de productos cargada";
                response.setSuccess(0);
            }
            else{
                msg = "The customer " + idClient + " does not have accounts yet";
                msgSpanish = "El cliente no cuenta con productos creados";
                response.setSuccess(1);
            }
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }

    //List of products different to the selected one
    @GetMapping("/customers/{idClient}/accounts/{idProduct}/different")
    public ResponseEntity<GeneralResponse<List<AccountEntity>>> listIdOtherAvailableProducts(@PathVariable("idClient") int idClient, @PathVariable("idProduct") int idProduct){
        GeneralResponse<List<AccountEntity>> response = new GeneralResponse<>();
        HttpStatus status = null;
        List<AccountEntity> data = null;

        try {
            data = accountService.listIdOtherAvailableProducts(idClient,idProduct);
            String msg;
            String msgSpanish;
            if (data.size()>0) {
                msg = "There was found " + data.size() + " accounts to transfer";
                msgSpanish = "Cuentas a transferir cargadas";
                response.setSuccess(0);
            }
            else{
                msg = "There are not accounts " + idClient + " to transfer money";
                msgSpanish = "No se disponen de cuentas disponibles para transferir";
                response.setSuccess(1);
            }
            response.setMessage(msg);
            response.setMessage(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }


    //Create a new product for a cliente
    @PostMapping("/customers/{idClient}/accounts")
    @ResponseBody
    public ResponseEntity<GeneralResponse<AccountEntity>> save(@RequestBody AccountEntity product, @PathVariable("idClient") int idClient, TransactionEntity transaction){
        GeneralResponse<AccountEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        AccountEntity data = null;

        try {
            product.setIdClient(idClient);
            transaction.setIdPrincipalProduct(product.getIdProduct());
            transaction.setDescription("Creación producto");
            transaction.setResultOperation("Efectiva");
            transaction.setFinalBalance(0);
            transaction.setValueOperation(0);
            transaction.setTypeOperation("Creación cuenta");
            transaction.setDateOperation(transaction.getDateOperation());
            transactionService.createTransaction(transaction, product.getIdProduct());

            data = accountService.addProduct(product, idClient);
            String msg;
            String msgSpanish;
            if (data != null) {
                msg = "Product was successfully created with id " + data.getIdProduct();
                msgSpanish = "Cuenta creada exitosamente";
                response.setSuccess(0);
            }
            else{
                msg = "Product was not created";
                msgSpanish = "Product no creado";
                response.setSuccess(1);
            }

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }

    //Get one product of a client
    @GetMapping("/accounts/{idProduct}")
    public ResponseEntity<GeneralResponse<AccountEntity>> ListIdOneProduct(@PathVariable("idProduct") int idProduct){
        GeneralResponse<AccountEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        AccountEntity data = null;

        try {
            data = accountService.listIdOneProduct(idProduct);
            String msg;
            String msgSpanish;
            if (data != null) {
                msg = "Account listed";
                msgSpanish = "Cuenta cargada";
                response.setSuccess(0);
            }
            else{
                msg = "Account not charged";
                msgSpanish = "Cuenta no encontrada";
                response.setSuccess(1);
            }
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }

    //Change status to active or inactive
    @PutMapping("/customers/{idClient}/accounts/{idProduct}/changeStatus")
    public ResponseEntity<GeneralResponse<AccountEntity>> changeStatus(AccountEntity product, @PathVariable("idProduct") int idProduct){
        GeneralResponse<AccountEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        AccountEntity data = null;

        try {
            product = ListIdOneProduct(idProduct).getBody().getData();
            product.setIdProduct(idProduct);
            product.setIdClient(product.getIdClient());
            product.setTypeAccount(product.getTypeAccount());
            product.setNumberAccount(product.getNumberAccount());
            product.setCreationDate(product.getCreationDate());
            product.setBalance(product.getBalance());

            String msg;
            String msgSpanish;
            if (product.getState().equals("activa"))
            {
                product.setState("inactiva");
                msg = "Account inactivated";
                msgSpanish = "Cuenta inactivada";
                response.setSuccess(0);
            }
            else if (product.getState().equals("inactiva"))
            {
                product.setState("activa");
                msg = "Account activated";
                msgSpanish = "Cuenta activada";
                response.setSuccess(0);
            }
            else
            {
                product.setState("Cancelado");
                msg = "the account is already canceled so it is not possible to switched";
                msgSpanish = "Cuenta cancelada. No puede ser activada o inactivada";
                response.setSuccess(1);
            }
            data = accountService.changeStatus(product);
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }

    //Change status to cancell product
    @PutMapping("/customers/{idClient}/accounts/{idProduct}/cancel")
    public ResponseEntity<GeneralResponse<AccountEntity>> cancelProduct(AccountEntity product, @PathVariable("idProduct") int idProduct){
        GeneralResponse<AccountEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        AccountEntity data = null;

        try {
            product = ListIdOneProduct(idProduct).getBody().getData();
            product.setIdProduct(idProduct);
            product.setIdClient(product.getIdClient());
            product.setTypeAccount(product.getTypeAccount());
            product.setNumberAccount(product.getNumberAccount());
            product.setCreationDate(product.getCreationDate());
            product.setBalance(product.getBalance());

            String msg;
            String msgSpanish;
            if (product.getBalance()!=0)
            {
                product.setState(product.getState());
                msg = "Account can not be cancelled because balance is different than zero. " +
                        "Balance is: " + product.getBalance();
                msgSpanish = "La cuenta no puede ser cancelada ya que su saldo no es cero. " +
                "Saldo de la cuenta: " + product.getBalance();
                response.setSuccess(0);
            }
            else{
                product.setState("Cancelado");
                msg = "Account successfully cancelled";
                msgSpanish = "Cuenta cancelada exitosamente";
                response.setSuccess(1);
            }
            data = accountService.cancelProduct(product);
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }

    //Update product balance
    @PutMapping("/customers/{idClient}/accounts/{idProduct}")
    public ResponseEntity<GeneralResponse<AccountEntity>> updateBalance(AccountEntity product, @PathVariable("idProduct") int idProduct, int finalBalance){
        GeneralResponse<AccountEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        AccountEntity data = null;

        try {
            product = ListIdOneProduct(idProduct).getBody().getData();
            product.setBalance(finalBalance);
            data = accountService.updateBalance(product);
            String msg;
            String msgSpanish;
            if (data != null) {
                msg = "Balance updated";
                msgSpanish = "Saldo actualizado exitosamente";
                response.setSuccess(0);
            }
            else{
                msg = "Balance not updated";
                msgSpanish = "Saldo no pudo ser actualizado";
                response.setSuccess(1);
            }

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }

}
