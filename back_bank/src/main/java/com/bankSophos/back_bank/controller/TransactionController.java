package com.bankSophos.back_bank.controller;

import com.bankSophos.back_bank.model.GeneralResponse;
import com.bankSophos.back_bank.service.AccountService;
import com.bankSophos.back_bank.service.TransactionService;
import com.bankSophos.back_bank.entity.AccountEntity;
import com.bankSophos.back_bank.entity.TransactionEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http:localhost:4200")
@RestController
@RequestMapping("/customers/{idClient}/accounts/{idProduct}")
public class TransactionController {

    public static final Logger logger = LoggerFactory.getLogger(CustomerController.class);


    @Autowired
    TransactionService transactionService;


    @Autowired
    AccountService accountService;


    //Calculate GMF
    @PostMapping("/GMF")
    @ResponseBody
    public ResponseEntity<GeneralResponse<TransactionEntity>> calculateGMF(@RequestBody TransactionEntity transaction, @PathVariable("idProduct") int idProduct){
        GeneralResponse<TransactionEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        TransactionEntity data = null;
        try{
            AccountEntity product = accountService.listIdOneProduct(idProduct);
            TransactionEntity transactionGMF = new TransactionEntity();
            transactionGMF.setIdPrincipalProduct(idProduct);
            transactionGMF.setValueOperation(-0.004 * transaction.getValueOperation());
            transactionGMF.setFinalBalance(product.getBalance() - 0.004 * transaction.getValueOperation());
            transactionGMF.setDescription("GMF 4 x 1000");
            transactionGMF.setResultOperation("Efectiva");
            transactionGMF.setDateOperation(transaction.getDateOperation());
            transactionGMF.setTypeOperation("GMF");
            transactionGMF.setFinanceMovement("Débito");
            product.setBalance(transactionGMF.getFinalBalance());
            accountService.updateBalance(product);
            data = transactionGMF;
//            transactionService.createTransaction(data, idProduct);
            String msg;
            String msgSpanish;
            if (data != null) {
                msg = "GMF calculated";
                msgSpanish = "GMF calculado";
                response.setSuccess(0);
            }
            else{
                msg = "GMF not calculated";
                msgSpanish = "Error al calcular el GMF";
                response.setSuccess(1);
            }
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;
        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }


    //Add money
    @PostMapping("/add")
    @ResponseBody
    public ResponseEntity<GeneralResponse<TransactionEntity>> addMoney(@RequestBody TransactionEntity transaction, @PathVariable("idProduct") int idProduct){
        GeneralResponse<TransactionEntity> response = new GeneralResponse<>();
        HttpStatus status = null;
        TransactionEntity data = null;
        try{

            AccountEntity product = accountService.listIdOneProduct(idProduct);
            transaction.setIdPrincipalProduct(idProduct);
            transaction.setFinalBalance(product.getBalance() + transaction.getValueOperation());
            transaction.setValueOperation(transaction.getValueOperation());
            transaction.setResultOperation("Efectiva");
            transaction.setFinanceMovement("Crédito");
            product.setBalance(transaction.getFinalBalance());
            accountService.updateBalance(product);
            data = transaction;
            transactionService.createTransaction(data, idProduct);
            String msg;
            String msgSpanish;
            if (data != null) {
                msg = "Money add to the account id " + transaction.getIdPrincipalProduct();
                msgSpanish = "Dinero consignado exitosamente";
                response.setSuccess(0);
            }
            else{
                msg = "Money was not add to the account";
                msgSpanish = "El dinero no fue consignado a la cuenta";
                response.setSuccess(1);
            }
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);
    }


    //Withdraw money
    @PostMapping("/withdrawal")
    @ResponseBody
    public ResponseEntity<GeneralResponse<TransactionEntity[]>> withdrawMoney(@RequestBody TransactionEntity transaction, @PathVariable("idProduct") int idProduct){
        GeneralResponse<TransactionEntity[]> response = new GeneralResponse<>();
        HttpStatus status = null;
        TransactionEntity data[] = new TransactionEntity[2];
        try{
            String msg;
            String msgSpanish;
            AccountEntity productWithdraw = accountService.listIdOneProduct(idProduct);
            if (productWithdraw.getState().equals("Cancelado")){
                msg = "Account is currently cancel so it is not possible withdraw money from it";
                msgSpanish = "La cuenta se encuentra cancelada por lo que no es posible retirar dinero";
                response.setSuccess(3);
            }
            else if (productWithdraw.getState().equals("inactiva")){
                msg = "Account is inactive so it is not possible withdraw money. First you need to activate the account";
                msgSpanish = "La cuenta se encuentra inactiva por lo que no es posible retirar dinero";
                response.setSuccess(3);
            }
            else{
                boolean balance = isEnoughMoney(transaction,idProduct);
                if (balance) {

                    transaction.setIdPrincipalProduct(idProduct);
                    transaction.setIdSecondaryProduct(transaction.getIdSecondaryProduct());
                    transaction.setFinalBalance(productWithdraw.getBalance() - transaction.getValueOperation());
                    transaction.setValueOperation(-transaction.getValueOperation());
                    transaction.setResultOperation("Efectiva");
                    transaction.setFinanceMovement("Débito");
                    transaction.setGMF(0.004 * transaction.getValueOperation());
                    productWithdraw.setBalance(transaction.getFinalBalance());
                    accountService.updateBalance(productWithdraw);
                    transactionService.createTransaction(transaction, idProduct);
                    data[0] = transaction;
                    transaction.setValueOperation(-transaction.getValueOperation());
                    data [1] = calculateGMF(transaction, idProduct).getBody().getData();
                    transactionService.createTransaction(data[1], idProduct);
                    msg = "Money successfully withdraw";
                    msgSpanish = "Dinero retirado exitosamente";
                    response.setSuccess(0);
                }
                else
                {
                    msg = "Insufficient balance";
                    msgSpanish = "Saldo insuficiente";
                    response.setSuccess(2);
                }
            }
            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;

        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);
    }


//    Transfer money
    @PostMapping("/transfer")
    @ResponseBody
    public ResponseEntity<GeneralResponse<TransactionEntity[]>> transferMoney(@RequestBody TransactionEntity transaction, @PathVariable("idProduct") int idProduct){
        GeneralResponse<TransactionEntity[]> response = new GeneralResponse<>();
        HttpStatus status = null;
        TransactionEntity data[] = new TransactionEntity[3];
        TransactionEntity transactionAdd = new TransactionEntity();

        try{
            String msg;
            String msgSpanish;
            AccountEntity productWithdraw = accountService.listIdOneProduct(idProduct);
            if (productWithdraw.getState().equals("Cancelado")){
                msg = "Account is currently cancel so it is not possible withdraw money from it";
                msgSpanish = "La cuenta se encuentra cancelada por lo que no es posible retirar dinero";
                response.setSuccess(3);
            }
            else if (productWithdraw.getState().equals("inactiva")){
                msg = "Account is currently inactive so it is not possible withdraw money from it";
                msgSpanish = "La cuenta se encuentra cancelada por lo que no es posible retirar dinero";
                response.setSuccess(3);
            }
            else {
                boolean balance = isEnoughMoney(transaction,idProduct);
                if (balance) {
                    TransactionEntity withdraw [] = withdrawMoney(transaction,idProduct).getBody().getData();
                    data[0] = withdraw[0];
                    data[1] = withdraw[1];
                    transactionService.createTransaction(data[0], idProduct);
                    transactionService.createTransaction(data[1], idProduct);
                    System.out.println(data[0].getIdTransaction());
                    System.out.println(data[0].getTypeOperation());
                    System.out.println(data[0].getDescription());
                    System.out.println(data[0].getIdPrincipalProduct());
                    System.out.println(data[0].getValueOperation());
                    System.out.println(data[0].getFinalBalance());
                    System.out.println(data[0].getResultOperation());
                    System.out.println(data[1].getIdTransaction());
                    System.out.println(data[1].getTypeOperation());
                    System.out.println(data[1].getDescription());
                    System.out.println(data[1].getIdPrincipalProduct());
                    System.out.println(data[1].getValueOperation());
                    System.out.println(data[1].getFinalBalance());
                    System.out.println(data[1].getResultOperation());


                    transactionAdd.setIdPrincipalProduct(transaction.getIdSecondaryProduct());
                    transactionAdd.setDescription("Recepción desde producto: " + transaction.getIdPrincipalProduct());
                    transactionAdd.setTypeOperation("Recepción Transferencia");
                    transactionAdd.setIdSecondaryProduct(idProduct);
                    transactionAdd.setDateOperation(transaction.getDateOperation());
                    transactionAdd.setValueOperation(transaction.getValueOperation());
                    addMoney(transactionAdd, transaction.getIdSecondaryProduct());
                    data[2] = transactionService.createTransaction(transactionAdd, idProduct);
                    transactionService.createTransaction(data[2], idProduct);

                    System.out.println(data[0].getIdTransaction());
                    System.out.println(data[0].getTypeOperation());
                    System.out.println(data[0].getDescription());
                    System.out.println(data[0].getIdPrincipalProduct());
                    System.out.println(data[0].getValueOperation());
                    System.out.println(data[0].getFinalBalance());
                    System.out.println(data[0].getResultOperation());
                    System.out.println(data[1].getIdTransaction());
                    System.out.println(data[1].getTypeOperation());
                    System.out.println(data[1].getDescription());
                    System.out.println(data[1].getIdPrincipalProduct());
                    System.out.println(data[1].getValueOperation());
                    System.out.println(data[1].getFinalBalance());
                    System.out.println(data[1].getResultOperation());
                    System.out.println(data[2].getIdTransaction());
                    System.out.println(data[2].getTypeOperation());
                    System.out.println(data[2].getDescription());
                    System.out.println(data[2].getIdPrincipalProduct());
                    System.out.println(data[2].getValueOperation());
                    System.out.println(data[2].getFinalBalance());
                    System.out.println(data[2].getResultOperation());

                    msg = "Money successfully transfer from " + transaction.getIdPrincipalProduct() + " to " + transaction.getIdSecondaryProduct();
                    msgSpanish = "Transferencia exitosa";
                    response.setSuccess(0);
                } else {
                    msg = "Insufficient balance";
                    msgSpanish = "Saldo insuficiente";
                    response.setSuccess(2);
                }
            }

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;
        } catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);
    }


    public boolean isEnoughMoney (@RequestBody TransactionEntity transaction, @PathVariable("idProduct") int idProduct){
        try{
            AccountEntity product = accountService.listIdOneProduct(idProduct);
            if (product.getState().equals("Cancelado")){
                return false;
            }
            else if (product.getTypeAccount().equals("corriente") && product.getBalance() - (1.004 * transaction.getValueOperation()) >= -2000000){
                return true;
            }
            else if (product.getTypeAccount().equals("ahorros") && product.getBalance() - (1.004 * transaction.getValueOperation()) >= 0){
                return true;
            }
            else{
                return false;
            }
        } catch (Exception e){
            return false;
        }
    }


    //Get transaction effective by product, by client
    @GetMapping("")
    public ResponseEntity<GeneralResponse<List<TransactionEntity>>> listIdTransaction(@PathVariable("idProduct") int idPrincipalProduct){
        GeneralResponse<List<TransactionEntity>> response = new GeneralResponse<>();
        HttpStatus status = null;
        List<TransactionEntity> data = null;
        try{
            data = transactionService.listIdTransaction(idPrincipalProduct);
            String msg;
            String msgSpanish;
            if (data.size() > 0) {
                msg = "Transaction listed";
                msgSpanish = "Transaciones efectivas por producto cargadas";
                response.setSuccess(0);
            }
            else{
                msg = "There was not transaction for this client";
                msgSpanish = "No se encuentran transacciones para este cliente";
                response.setSuccess(1);
            }

            response.setMessage(msg);
            response.setMsgSpanish(msgSpanish);
            response.setData(data);
            status = HttpStatus.OK;
        }catch (Exception e) {

            String msg = "Something has failed. Please contact support.";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setMessage(msg);
            response.setSuccess(4);

        }
        return new ResponseEntity<>(response, status);

    }


}
