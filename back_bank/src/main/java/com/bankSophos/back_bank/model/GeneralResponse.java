package com.bankSophos.back_bank.model;

import java.io.Serializable;

public class GeneralResponse <T> implements Serializable {

    private T data;
    private int success;
    private String message;
    private String msgSpanish;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int isSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSuccess() {
        return success;
    }

    public String getMsgSpanish() {
        return msgSpanish;
    }

    public void setMsgSpanish(String msgSpanish) {
        this.msgSpanish = msgSpanish;
    }

}
