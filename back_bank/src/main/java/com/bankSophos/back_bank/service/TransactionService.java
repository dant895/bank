package com.bankSophos.back_bank.service;

import com.bankSophos.back_bank.entity.TransactionEntity;

import java.util.List;

public interface TransactionService {
    public TransactionEntity createTransaction(TransactionEntity transaction, int idPrincipalProduct)  throws Exception;
    public boolean isEnoughMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception;
    public TransactionEntity addMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception;
    public TransactionEntity withdrawMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception;
    public TransactionEntity transferMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception;
    public TransactionEntity calculateGMF(TransactionEntity transaction, int idPrincipalProduct) throws Exception;
    public List<TransactionEntity> listIdTransaction(int idPrincipalProduct)  throws Exception;
}
