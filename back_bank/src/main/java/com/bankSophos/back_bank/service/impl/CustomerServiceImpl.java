package com.bankSophos.back_bank.service.impl;

import com.bankSophos.back_bank.service.CustomerService;
import com.bankSophos.back_bank.repository.CustomerRepository;
import com.bankSophos.back_bank.entity.CustomerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository dataCustomer;

    @Override
    public List<CustomerEntity> list() throws Exception{
        return (List<CustomerEntity>) dataCustomer.findAll();
    }

    @Override
    public CustomerEntity listIdOneClient(int id) throws Exception  {
        return dataCustomer.findById(id);
    }

    @Override
    public CustomerEntity add(CustomerEntity c) throws Exception  {
        return dataCustomer.save(c);
    }

    @Override
    public CustomerEntity edit(CustomerEntity c) throws Exception  {
        return dataCustomer.save(c);
    }

    @Override
    public void delete(int id) throws Exception  {
        dataCustomer.deleteById(id);
    }
}
