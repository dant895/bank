package com.bankSophos.back_bank.service;

import com.bankSophos.back_bank.entity.CustomerEntity;

import java.util.List;

public interface CustomerService {
    public List<CustomerEntity> list() throws Exception;
    public CustomerEntity listIdOneClient(int id) throws Exception;
    public CustomerEntity add(CustomerEntity c) throws Exception;
    public CustomerEntity edit(CustomerEntity c) throws Exception;
    public void delete(int id) throws Exception;
}
