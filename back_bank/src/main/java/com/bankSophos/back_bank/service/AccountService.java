package com.bankSophos.back_bank.service;

import com.bankSophos.back_bank.entity.AccountEntity;

import java.util.List;

public interface AccountService {
    public List<AccountEntity> listIdProduct(int idClient) throws Exception;
    public AccountEntity listIdOneProduct(int idProduct) throws Exception;
    public AccountEntity addProduct(AccountEntity product, int idClient) throws Exception;
    public AccountEntity changeStatus(AccountEntity product) throws Exception;
    public AccountEntity updateBalance(AccountEntity product) throws Exception;
    public List<AccountEntity> listIdOtherAvailableProducts(int idClient, int idProduct) throws Exception;
    public AccountEntity cancelProduct(AccountEntity product) throws Exception;

}
