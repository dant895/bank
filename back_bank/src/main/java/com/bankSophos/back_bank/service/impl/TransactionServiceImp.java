package com.bankSophos.back_bank.service.impl;

import com.bankSophos.back_bank.service.TransactionService;
import com.bankSophos.back_bank.repository.TransactionRepository;
import com.bankSophos.back_bank.entity.TransactionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImp implements TransactionService {

    @Autowired
    private TransactionRepository dataTransaction;

    @Override
    public TransactionEntity createTransaction(TransactionEntity transaction, int idPrincipalProduct)  throws Exception{
        return dataTransaction.save(transaction);
    }

    @Override
    public boolean isEnoughMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception{
        return false;
    }

    @Override
    public TransactionEntity addMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception{
        return dataTransaction.save(transaction);
    }

    @Override
    public TransactionEntity withdrawMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception{
        return dataTransaction.save(transaction);
    }

    @Override
    public TransactionEntity transferMoney(TransactionEntity transaction, int idPrincipalProduct)  throws Exception{
        return dataTransaction.save(transaction);
    }

    @Override
    public TransactionEntity calculateGMF(TransactionEntity transaction, int idPrincipalProduct) throws Exception{
        return dataTransaction.save(transaction);
    }

    @Override
    public List<TransactionEntity> listIdTransaction(int idPrincipalProduct)  throws Exception{
        return dataTransaction.findByIdPrincipalProductAndResultOperation(idPrincipalProduct, "Efectiva");
    }


}
