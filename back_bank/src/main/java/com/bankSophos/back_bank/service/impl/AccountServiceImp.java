package com.bankSophos.back_bank.service.impl;

import com.bankSophos.back_bank.service.AccountService;
import com.bankSophos.back_bank.service.TransactionService;
import com.bankSophos.back_bank.repository.AccountRepository;
import com.bankSophos.back_bank.entity.AccountEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AccountServiceImp implements AccountService {

    @Autowired
    private AccountRepository dataProduct;

    @Autowired
    TransactionService transactionService;

    @Override
    public List<AccountEntity> listIdProduct(int idClient) throws Exception {
        return dataProduct.findByIdClient(idClient);
    }

    @Override
    public AccountEntity listIdOneProduct(int idProduct) throws Exception {
        return dataProduct.findByIdProduct(idProduct);
    }

    @Override
    public AccountEntity addProduct(AccountEntity product, int idClient) throws Exception {
        return dataProduct.save(product);
    }

    @Override
    public AccountEntity changeStatus(AccountEntity product) throws Exception {
        return dataProduct.save(product);
    }

    @Override
    public AccountEntity updateBalance(AccountEntity product) throws Exception {
        return dataProduct.save(product);
    }

    @Override
    public List<AccountEntity> listIdOtherAvailableProducts(int idClient, int idProduct) throws Exception {
        return dataProduct.findByStateNotAndIdClientNotAndIdProductNot("Cancelado", idClient, idProduct);
    }

    @Override
    public AccountEntity cancelProduct(AccountEntity product) throws Exception {
        return dataProduct.save(product);
    }

}
