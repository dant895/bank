package com.bankSophos.back_bank.service.impl;

import com.bankSophos.back_bank.entity.UserEntity;
import com.bankSophos.back_bank.repository.UserRepository;
import com.bankSophos.back_bank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImp implements UserService, UserDetailsService {

    @Autowired
    private UserRepository dataUser;

    @Override
    public List<UserEntity> get() throws Exception {
        return dataUser.findAll();
    }


    @Override
    public UserEntity save(UserEntity users) throws Exception {
        return dataUser.save(users);
    }

    @Override
    public void delete(String username) throws Exception {
        dataUser.deleteById(username);

    }

    @Override
    public UserEntity getUser(String username) throws Exception {
        return dataUser.findByUserName(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userV0 = dataUser.findByUserName(username);
        List<GrantedAuthority> roles = new ArrayList<>();
//        roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        UserDetails userDetails = new User(userV0.getUserName(), userV0.getPassword(), roles);
        return userDetails;
    }


}
