package com.bankSophos.back_bank.service;

import com.bankSophos.back_bank.entity.UserEntity;

import java.util.List;

public interface UserService {

    public List<UserEntity> get() throws Exception;
    public UserEntity save(UserEntity users) throws Exception;
    public void delete(String username) throws Exception;
    public UserEntity getUser(String username) throws Exception;
}
